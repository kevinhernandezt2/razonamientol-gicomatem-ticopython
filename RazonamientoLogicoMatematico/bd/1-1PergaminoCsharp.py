import json

#json diccionario de dato gemas
gemas = [
    {
        'id_soldado': 1111,
        'gemas': True
    },
    {
        'id_soldado': 1112,
        'gemas': False
    },
    {
        'id_soldado': 1113,
        'gemas': True
    },
    {
        'id_soldado': 1114,
        'gemas': False
    }
]
   
#json diccionario de dato gemasinfo
gemasinfo = [
    
    {
        'tipo_gema': 'onepiece',
        'poder_gema': 50,
        'tipo_arma': 'cuchillos'
    },
    {
        'tipo_gema': 'secondpiece',
        'poder_gema': 100,
        'tipo_arma': 'lanzas'
    },
    {
        'tipo_gema': 'thirdpiece',
        'poder_gema': 150,
        'tipo_arma': 'mazas o martillos'
    },
    {
        'tipo_gema': 'fourpiece',
        'poder_gema': 200,
        'tipo_arma': 'espadas'
    }

]
   
#json diccionario de dato armasinfo
armasinfo = [
    {
        'nombre_arma': 'cuchillos',
        'poder_arma': 20,
        'posicion': 1
    },
    {
        'nombre_arma': 'lanzas',
        'poder_arma': 40,
        'posicion': 2
    },
    {
        'nombre_arma': 'mazas o martillos',
        'poder_arma': 60,
        'posicion': 3
    },
    {
        'nombre_arma': 'espadas',
        'poder_arma': 100,
        'posicion': 4
    }
       
]
#json diccionario de dato dinastias
dinastias = [
    {
        'nombre_dinastia': 'sw-ift',
        'poder_x_posicion': [1,2]
    },
    {
        'nombre_dinastia': 'flu-tter',
        'poder_x_posicion': [3,4]
    },
    {
        'nombre_dinastia': 'java',
        'poder_x_posicion': [1,2]
    }
]
#json diccionario de dato position
position = [
    {
        'posicion': [1,2],
        'poder': 'positivo'
    },
    {
        'posicion': [3,4],
        'poder': 'negativo'
    }
]
#json diccionario de dato pergamino json que se añadio campos
pergamino = {
    'gem-name': ['onepiece','secondpiece','thirdpiece','fourpiece'],
    'armas-name': ['cuchillos','lanzas','mazas o martillos','espadas']
}

#json o dicionario de dato, posibles combinaciones y sumas para soldado 1111 y soldado 1113 que poseen gemas 
combination = {
    f"{armasinfo[0]['nombre_arma']},{armasinfo[0]['posicion']},{gemasinfo[0]['tipo_gema']},{dinastias[0]['nombre_dinastia']}": (armasinfo[0]['poder_arma'] + gemasinfo[0]['poder_gema']),
    f"{armasinfo[1]['nombre_arma']},{armasinfo[1]['posicion']},{gemasinfo[1]['tipo_gema']},{dinastias[0]['nombre_dinastia']}": (armasinfo[1]['poder_arma'] + gemasinfo[1]['poder_gema']),
    f"{armasinfo[0]['nombre_arma']},{armasinfo[0]['posicion']},{gemasinfo[0]['tipo_gema']},{dinastias[2]['nombre_dinastia']}": (armasinfo[0]['poder_arma'] + gemasinfo[0]['poder_gema']),
    f"{armasinfo[1]['nombre_arma']},{armasinfo[1]['posicion']},{gemasinfo[1]['tipo_gema']},{dinastias[2]['nombre_dinastia']}": (armasinfo[1]['poder_arma'] + gemasinfo[1]['poder_gema']),
    f"{armasinfo[2]['nombre_arma']},{armasinfo[2]['posicion']},{gemasinfo[2]['tipo_gema']},{dinastias[1]['nombre_dinastia']}": f"{gemasinfo[2]['poder_gema']}",
    f"{armasinfo[3]['nombre_arma']},{armasinfo[3]['posicion']},{gemasinfo[3]['tipo_gema']},{dinastias[1]['nombre_dinastia']}": f"{gemasinfo[3]['poder_gema']}"
}

#variables para pasarlas a un json y pasarle como parametro el json o diccionario de datos
cadena_gema = json.dumps(gemas)
cadena_gemasinfo = json.dumps(gemasinfo)
cadena_armasinfo = json.dumps(armasinfo)
cadena_dinastias = json.dumps(dinastias)
cadena_position = json.dumps(position)
cadena_pergamino = json.dumps(pergamino)
cadena_combination = json.dumps(combination)

"""
#Escritura de directorios pasandolo el json para poder que se generen en los directorios
with open("C:\\RazonamientoLogicoMatematico\\bd\combinaciones\soldadoid1113\combination.json", "w") as c:
    c.write(cadena_combination)
"""


#deber´as poder leer cada archivo de la poderosa base del conocimiento (ama-zon) del reino de py-thon y asociar dicha informaci´on con cada soldado enemigo


for gem in gemas:
    
    if gem['gemas'] == True:

        print("****** SOLDADOS CON GEMA************\n")
        print(f"soldado id:\n")
        print(f"{gem['id_soldado']}\n")
        print("****** INFORMACION DE LAS GEMAS QUE CONTIENE EL SOLDADO************\n")

        for geminfo in gemasinfo:
            print(f"tipoGema: {geminfo['tipo_gema']}, poderGema: {geminfo['poder_gema']}, tipoArma asociada: {geminfo['tipo_arma']}\n\n")
            
        for arminfo in armasinfo:
            print("****** INFORMACION DE ARMA - SE PODRA OBSERVAR EL ARMA ASOCIADA A LA GEMA QUE CONTIENE EL SOLDADO************\n")
            print(f"nombreArma: {arminfo['nombre_arma']}, poderArma: {arminfo['poder_arma']}, posicion: {arminfo['posicion']}\n\n")

print("------------------------------------------------------------------------------------\n")
for din in dinastias:
    print(f"********* DINASTIA ASOCIADAS A UN SOLDADO DE ACUERDO A SU PODER POR POSICION\n")
    print(f"nombreDinastia: {din['nombre_dinastia']}, poderporposicion: {din['poder_x_posicion']}\n")

print("------------------------------------------------------------------------------------\n")
for pos in position:
    
    if pos['poder'] == "positivo":
        print("********* POSICIONES QUE REPRESENTAN PODER POSITIVO PARA SU DINASTIA Y ASI MISMO PARA SU ARMA DE CADA SOLDADO\n")
        print(f"posicion: {pos['posicion']}\n")
    if pos['poder'] == "negativo":
        print("********* POSICIONES QUE REPRESENTAN PODER NEGATIVO PARA SU DINASTIA Y ASI MISMO EL PODER DEL ARMA NO SUMARIA DE CADA SOLDADO\n")
        print(f"posicion: {pos['posicion']}\n")










