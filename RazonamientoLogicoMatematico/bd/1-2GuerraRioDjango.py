"""
Tu misi´on ser´a programar la poderosa tableta mn-zinc para poder ayudar a nuestro protagonista, deber´as escribir
un programa en seudoc´odigo que permita a nuestro protagonista derrotar al ej´ercito del general fla-sk.
leyendo la poderosa base de conocimiento del reino de
py-thon (ama-zon) debes leer cada directorio del ejercito
enemigo que se ha creado en el problema anterior y poder calcular cual ser´ıa la combinaci´on que dar´ıa el m´aximo
poder del ejercito del general fla-sk debido a que nuestro
protagonista ingenuamente formara un ej´ercito el doble de
poderoso para poder derrotarlo.
"""

import json

#variables identificar directorio

directorio_gemas = "C:\\RazonamientoLogicoMatematico\\bd\people\\apodo\gemas.json"
directorio_gemasinfo = "C:\\RazonamientoLogicoMatematico\\bd\gemas\gemaid\gemasinfo.json"
directorio_armasinfo = "C:\\RazonamientoLogicoMatematico\\bd\\arma\\tipoarmaid\\armasinfo.json"
directorio_dinastias = "C:\\RazonamientoLogicoMatematico\\bd\\arma\dinastiasid\dinastias.json"
directorio_posicion = "C:\\RazonamientoLogicoMatematico\\bd\\arma\dinastiasid\position.json"
directorio_combinacionsoldado1111 = "C:\\RazonamientoLogicoMatematico\\bd\combinaciones\soldadoid1111\combination.json"
directorio_combinacionsoldado1113 = "C:\\RazonamientoLogicoMatematico\\bd\combinaciones\soldadoid1113\combination.json"
directorio_pergamino = "C:\\RazonamientoLogicoMatematico\\bd\pergamino.json"

#lectura directorio del ejercito enemigo para poder hacer calculo
with open(directorio_gemas, "r") as g:
   lee_gemas = g.read()

objeto_gemas = json.loads(lee_gemas)
print("directorio json gemas\n\n" + str(objeto_gemas) + "\n\n")

with open(directorio_gemasinfo, "r") as gi:
   lee_gemas_info = gi.read()

objeto_gemas_info = json.loads(lee_gemas_info)
print("directorio json gemas info\n\n" + str(objeto_gemas_info) + "\n\n") 

with open(directorio_armasinfo, "r") as ai:
   lee_armas_info = ai.read()

objeto_armas_info = json.loads(lee_armas_info)
print("directorio json armas info\n\n" + str(objeto_armas_info) + "\n\n")

with open(directorio_dinastias, "r") as d:
   lee_dinastias = d.read()

objeto_dinastias = json.loads(lee_dinastias)
print("directorio json dinastias info\n\n" + str(objeto_dinastias) + "\n\n")

with open(directorio_posicion, "r") as p:
   lee_posicion = p.read()

objeto_posicion = json.loads(lee_posicion)
print("directorio json posicion info\n\n" + str(objeto_posicion) + "\n\n")

with open(directorio_combinacionsoldado1111, "r") as cs1111:
   lee_soldado1111 = cs1111.read()

objeto_soldado1111 = json.loads(lee_soldado1111)
print("directorio json combinacion soldado 1111 info\n\n" + str(objeto_soldado1111) + "\n\n")

with open(directorio_combinacionsoldado1113, "r") as cs1113:
   lee_soldado1113 = cs1113.read()

objeto_soldado1113 = json.loads(lee_soldado1113)
print("directorio json combinacion soldado 1113 info\n\n" + str(objeto_soldado1113) + "\n\n")

with open(directorio_pergamino, "r") as per:
   lee_pergamino = per.read()

objeto_pergamino = json.loads(lee_pergamino)
print("directorio json pergamino info\n\n" + str(objeto_pergamino) + "\n\n")

#de acuerdo a la lectura de los directorios de la base de conocimiento y en especifico de los directorios de combinaciones de soldados,
#se calcula cual ser´ıa la combinaci´on que dar´ıa el m´aximo poder del ejercito del general fla-sk

print(f"Combinacion maxima de poder de acuerdo a sus combinaciones y sumas:\n 'espadas,4,fourpiece,flu-tter' : {objeto_soldado1111['espadas,4,fourpiece,flu-tter']}")



    


